﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Duck.Models;
using Duck.Utils.UIExtensionClasses;

namespace Duck.Core
{
    public class Scene
    {
        public Camera Camera { get; set; }
        public ObservableCollection<SceneObject> SceneObjects { get; set; }

        public WaterModel Water;
        public EnviromentModel Enviroment;
        public LightModel Light;
        public DuckModel Duck;
        #region Commands



        #endregion

        public Scene()
        {
            Camera = new Camera();
            SceneObjects = new ObservableCollection<SceneObject>();
        }

        public WaterModel AddWaterModel()
        {
            Water = new WaterModel();
            SceneObjects.Add(Water);
            return Water;
        }

        public EnviromentModel AddEnviromentModel()
        {
            Enviroment = new EnviromentModel();
            SceneObjects.Add(Enviroment);
            return Enviroment;
        }

        public CursorModel AddCursorModel()
        {
            var item = new CursorModel();
            SceneObjects.Add(item);
            return item;
        }

        public DuckModel AddDuckModel()
        {
            Duck = new DuckModel();
            SceneObjects.Add(Duck);
            return Duck;
        }


        public LightModel AddLightModel()
        {
            Light = new LightModel();
            SceneObjects.Add(Light);
            return Light;
        }

        public void RemoveSceneObject(SceneObject item)
        {
            if (item != null)
            {
                SceneObjects.Remove(item);
                item.Dispose();
            }
        }
    }
}