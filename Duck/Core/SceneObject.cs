﻿using System;
using System.Collections.Generic;
using Duck.Components;
using Duck.Utils.UIExtensionClasses;

namespace Duck.Core
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }

    public class SceneObject : BindableObject, IDisposable
    {
        public string ObjectName { get; set; }
        public bool IsVisible { get; set; } = true;
        public Transform Transform;
        private List<Component> _components;

        #region UI Related

        public bool IsVisibleOnList
        {
            get { return _isVisibleOnList; }
            set
            {
                _isVisibleOnList = value;
                RaisePropertyChanged();
            }
        }
        private bool _isVisibleOnList = true;

        #endregion

        public SceneObject()
        {
            _components = new List<Component>();
            Transform = AddComponent<Transform>();
        }

        public TComponent AddComponent<TComponent>()
            where TComponent : Component, new()
        {
            TComponent component = new TComponent { Owner = this };
            _components.Add(component);
            return component;
        }

        public TComponent GetComponent<TComponent>()
            where TComponent : Component
        {
            foreach (var item in _components)
            {
                if (item is TComponent)
                {
                    return (TComponent)item;
                }
            }
            return null;
        }

        public void RemoveComponent(Component component)
        {
            _components.Remove(component);
        }


        protected virtual void OnRender()
        {

        }

        protected virtual void OnUpdate()
        {

        }

        public void Render()
        {
            if (!IsVisible)
                return;

            OnRender();
        }

        public virtual void Update()
        {
            OnUpdate();
        }

        public virtual void Dispose()
        {
        }
    }
}
