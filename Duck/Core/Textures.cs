﻿using System.Collections.Generic;
using Duck.OpenTK;

namespace Duck.Core
{
    public class Textures
    {

        private static List<string> _faces = new List<string>()
        {
            "./Textures/Environment/right.jpg",
            "./Textures/Environment/left.jpg",
            "./Textures/Environment/top.jpg",
            "./Textures/Environment/bottom.jpg",
            "./Textures/Environment/front.jpg",
            "./Textures/Environment/back.jpg"
        };

        private static Texture _cubeMapTexture;
        public static Texture CubeMapTexture
        {
            get
            {
                if (_cubeMapTexture == null)
                {
                    _cubeMapTexture = new Texture(_faces);
                    return _cubeMapTexture;
                }
                return _cubeMapTexture;
            }
        }

        private static Texture _duckTexture;
        public static Texture  DuckTexture
        {
            get
            {
                if (_duckTexture == null)
                {
                    _duckTexture = new Texture("./Textures/Duck/Body.jpg");
                    return _duckTexture;
                }
                return _duckTexture;
            }
        }

        private static Texture _testTexture;
        public static Texture TestTexture
        {
            get
            {
                if (_testTexture == null)
                {
                    _testTexture = new Texture("./Textures/WoodTex.jpg");
                    return _testTexture;
                }
                return _testTexture;
            }
        }
    }
}
