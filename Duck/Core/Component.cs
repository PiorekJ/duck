﻿using Duck.Utils.UIExtensionClasses;

namespace Duck.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }
    }
}
