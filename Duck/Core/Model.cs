﻿using System.Windows.Media;
using Duck.OpenTK;

namespace Duck.Core
{
    public interface IColorable
    {
        Color Color { get; set; }
    }

    public abstract class Model : SceneObject
    {
        public IMesh Mesh;
        public Shader Shader;

        public override void Dispose()
        {
            base.Dispose();
            Mesh?.Dispose();
        }


    }
}
