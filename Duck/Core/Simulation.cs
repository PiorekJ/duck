﻿using Duck.Utils;

namespace Duck.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static Settings Settings { get; set; }

        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        public Simulation()
        {
            Scene = new Scene();
            Settings = new Settings();
            TimeCounter = new TimeCounter();
        }

        public void InitializeSimulation()
        {
            TimeCounter.Start();
            Scene.AddLightModel();
            Scene.AddEnviromentModel();
            Scene.AddDuckModel();
            Scene.AddWaterModel();
        }
    }
}
