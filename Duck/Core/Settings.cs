﻿using Duck.Utils.UIExtensionClasses;

namespace Duck.Core
{
    public class Settings : BindableObject
    {
        public float DuckSpeed = 0.5f;
        public float SceneSize = 5.0f;
    }
}
