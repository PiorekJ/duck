﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace Duck.OpenTK
{
    public class Texture
    {
        public int TexID { get; private set; }

        public Texture(string filepath)
        {
            TexID = CreateFromFile(filepath);
        }

        public Texture(List<string> filepaths)
        {
            TexID = CreateCubeMapFromFile(filepaths);
        }

        private int CreateFromFile(string filepath)
        {
            Bitmap bitMap;
            try
            {
                bitMap = new Bitmap(filepath);
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Creating bitmap: " + e.Message);
            }

            BitmapData bitMapData = bitMap.LockBits(new Rectangle(0, 0, bitMap.Width, bitMap.Height),
                ImageLockMode.ReadOnly, bitMap.PixelFormat);
            var tex = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, bitMapData.Width, bitMapData.Height, 0, PixelFormat.Bgr, PixelType.UnsignedByte, bitMapData.Scan0);
            bitMap.UnlockBits(bitMapData);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            return tex;
        }

        private int CreateCubeMapFromFile(List<string> filepaths)
        {

            var tex = GL.GenTexture();
            GL.BindTexture(TextureTarget.TextureCubeMap, tex);
            for (int i = 0; i < filepaths.Count; i++)
            {
                Bitmap bitMap;
                string filepath = filepaths[i];
                try
                {
                    bitMap = new Bitmap(filepath);
                }
                catch (FileNotFoundException e)
                {
                    throw new FileNotFoundException("Creating bitmap: " + e.Message);
                }

                BitmapData bitMapData = bitMap.LockBits(new Rectangle(0, 0, bitMap.Width, bitMap.Height),
                    ImageLockMode.ReadOnly, bitMap.PixelFormat);

                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, PixelInternalFormat.Rgb, bitMapData.Width, bitMapData.Height, 0, PixelFormat.Bgr, PixelType.UnsignedByte, bitMapData.Scan0);
                bitMap.UnlockBits(bitMapData);
            }

            GL.TexParameterI(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, new[] { (int)TextureMinFilter.Linear });
            GL.TexParameterI(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, new[] { (int)TextureMagFilter.Linear });
            GL.TexParameterI(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, new[] { (int)TextureWrapMode.ClampToEdge });
            GL.TexParameterI(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, new[] { (int)TextureWrapMode.ClampToEdge });
            GL.TexParameterI(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, new[] { (int)TextureWrapMode.ClampToEdge });

            return tex;
        }
    }
}
