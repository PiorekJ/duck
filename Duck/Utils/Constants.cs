﻿using OpenTK;

namespace Duck.Utils
{
    public static class Constants
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 1, 5);
        public const float DefaultFOV = (float)System.Math.PI / 4;
        public const float DefaultZNear = 0.0001f;
        public const float DefaultZFar = 150f;

        public const float CameraMovementMouseSensitivity = 0.5f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 0.5f;

        public const float CameraMovementKeyVelocity = 5.0f;
        public const float CameraMovementKeySlowVelocity = 0.25f;

        public const float Epsilon = 0.00001f;
    }
}
