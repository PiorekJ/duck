﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Duck.OpenTK;
using OpenTK;

namespace Duck.Utils
{
    public static class MeshManager
    {
        private static readonly char[] delimiters = { ' ' };

        public static Mesh<VertexPNT> ReadMeshFile(string filename)
        {
            List<string> lines = File.ReadAllLines(filename).ToList();
            int carot = 1;
            int verticesPosCount = int.TryParse(lines[carot - 1], out verticesPosCount) ? verticesPosCount : 0;

            List<VertexPNT> vertices = new List<VertexPNT>();
            for (int i = carot; i < verticesPosCount + 1; i++)
            {
                var values = lines[i].Split(delimiters, 8);
                VertexPNT vertex = new VertexPNT(float.Parse(values[0], CultureInfo.InvariantCulture),
                    float.Parse(values[1], CultureInfo.InvariantCulture),
                    float.Parse(values[2], CultureInfo.InvariantCulture),
                    float.Parse(values[3], CultureInfo.InvariantCulture),
                    float.Parse(values[4], CultureInfo.InvariantCulture),
                    float.Parse(values[5], CultureInfo.InvariantCulture),
                    float.Parse(values[6], CultureInfo.InvariantCulture),
                    float.Parse(values[7], CultureInfo.InvariantCulture));

                vertices.Add(vertex);
            }

            carot += verticesPosCount + 1;
            int indiecesCount = int.TryParse(lines[carot - 1], out indiecesCount) ? indiecesCount : 0;
            List<uint> indieces = new List<uint>();
            for (int i = carot; i < carot + indiecesCount; i++)
            {
                var values = lines[i].Split(delimiters, 3);
                indieces.Add(uint.Parse(values[0]));
                indieces.Add(uint.Parse(values[1]));
                indieces.Add(uint.Parse(values[2]));
            }

            return new Mesh<VertexPNT>(vertices, indieces, MeshType.Triangles, AccessType.Static);
        }
    }
}
