﻿using System;
using OpenTK;

namespace Duck.Utils.Math
{
    public static class MathGeneralUtils
    {
        public static Matrix4 CreatePerspectiveFOVMatrix(float fov, float aspectRatio, float zNear, float zFar)
        {

            if (fov <= 0 || fov > System.Math.PI)
                throw new ArgumentOutOfRangeException("Incorrect fov");
            if (aspectRatio <= 0)
                throw new ArgumentOutOfRangeException("Incorrect aspect ratio");
            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("Incorrect zNear");

            float yMax = zNear * (float)System.Math.Tan(0.5f * fov);
            float yMin = -yMax;
            float xMin = yMin * aspectRatio;
            float xMax = yMax * aspectRatio;

            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("zNear");

            float x = (2.0f * zNear) / (xMax - xMin);
            float y = (2.0f * zNear) / (yMax - yMin);
            float a = (xMax + xMin) / (xMax - xMin);
            float b = (yMax + yMin) / (yMax - yMin);
            float c = -(zFar + zNear) / (zFar - zNear);
            float d = -(2.0f * zFar * zNear) / (zFar - zNear);

            return new Matrix4(x, 0, 0, 0,
                0, y, 0, 0,
                a, b, c, -1,
                0, 0, d, 0);
        }

        public static Matrix4 CreateTranslation(Vector3 position)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row3 = new Vector4(position.X, position.Y, position.Z, 1.0f);
            return result;
        }

        public static Matrix4 CreateFromQuaternion(Quaternion rotation)
        {
            rotation.ToAxisAngle(out var axis, out var angle);

            float cos = (float)System.Math.Cos(-angle);
            float sin = (float)System.Math.Sin(-angle);
            float t = 1.0f - cos;

            axis.Normalize();

            Matrix4 result = Matrix4.Identity;
            result.Row0 = new Vector4(t * axis.X * axis.X + cos, t * axis.X * axis.Y - sin * axis.Z, t * axis.X * axis.Z + sin * axis.Y, 0.0f);
            result.Row1 = new Vector4(t * axis.X * axis.Y + sin * axis.Z, t * axis.Y * axis.Y + cos, t * axis.Y * axis.Z - sin * axis.X, 0.0f);
            result.Row2 = new Vector4(t * axis.X * axis.Z - sin * axis.Y, t * axis.Y * axis.Z + sin * axis.X, t * axis.Z * axis.Z + cos, 0.0f);
            result.Row3 = Vector4.UnitW;
            return result;
        }

        public static Matrix4 CreateScale(Vector3 scale)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row0.X = scale.X;
            result.Row1.Y = scale.Y;
            result.Row2.Z = scale.Z;
            return result;
        }
    }
}
