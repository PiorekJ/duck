﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Duck.Core;
using OpenTK.Graphics.OpenGL;
using InputManager = Duck.Core.InputManager;

namespace Duck
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
            GL.LineWidth(1);
            Simulation.InitializeSimulation();

            CompositionTarget.Rendering += OnRender;
        }

        private void OnRender(object sender, EventArgs e)
        {
            Simulation.TimeCounter.CountDT();
            GL.ClearColor(System.Drawing.Color.Black);

            Simulation.Scene.Camera.UpdateCamera(DisplayControl.MousePosition);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (SceneObject item in Simulation.Scene.SceneObjects)
            {
                item.Update();
                item.Render();
            }

            //Simulation.Scene.Enviroment.Update();
            //Simulation.Scene.Enviroment.Render();

            DisplayControl.SwapBuffers();
        }



        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (SceneObject item in Simulation.Scene.SceneObjects)
            {
                item.Dispose();
            }
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }
    }
}
