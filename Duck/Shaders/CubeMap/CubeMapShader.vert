﻿#version 330 core

in layout(location = 0) vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 TexCoords;

void main()
{
	TexCoords = position/10.0f;
	gl_Position = projection * view * model * vec4(position,1);
}
