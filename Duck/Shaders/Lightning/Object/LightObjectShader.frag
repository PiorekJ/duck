﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in V_Out
{
	vec3 normal;
	vec3 color;
	vec3 fragPos;
} In;

uniform vec3 lightColor;
uniform vec3 lightPos;

void main()
{
	float ambientStrength = 0.2f;
	vec3 ambient = ambientStrength * In.color;

	vec3 norm = normalize(In.normal);
	vec3 lightDir = normalize(lightPos - In.fragPos);  

	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;	

	vec3 result = (ambient + diffuse) * In.color;
	FragColor = vec4(result, 1);
}
