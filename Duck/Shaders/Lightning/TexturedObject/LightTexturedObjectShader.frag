﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in V_Out
{
	vec3 normal;
	vec3 fragPos;
	vec2 texCoords;
} In;

uniform sampler2D tex;
uniform float alpha;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 camPos;
uniform float ks, kd, ka, m;

vec4 phong(vec3 worldPos, vec3 norm, vec3 view)
{
	view = normalize(view);
	norm = normalize(norm);
	vec3 tangent = vec3(norm.x, -norm.z, norm.y);
	vec3 color = texture(tex, In.texCoords).rgb * ka; //ambient
	vec3 lightVec = normalize(lightPos - worldPos);
	vec3 halfVec = normalize(view + lightVec);
	color += lightColor * kd * texture(tex, In.texCoords).rgb * clamp(dot(norm, lightVec), 0.0, 1.0);//diffuse
	float specDot = 1 - dot(tangent, halfVec) * dot(tangent, halfVec);
	color += lightColor * ks * pow(clamp(specDot, 0.0, 1.0), m);//specular
	return clamp(vec4(color, 1.0f), 0.0, 1.0);
}

void main()
{
	//float ambientStrength = 0.2f;
	//vec3 ambient = ambientStrength * texture(tex, In.texCoords).xyz * color;
	//vec3 norm = normalize(In.normal);
	//vec3 lightDir = normalize(lightPos - In.fragPos);  
	//float diff = max(dot(norm, lightDir), 0.0);
	//vec3 diffuse = diff * lightColor;	
	//vec3 result = (ambient + diffuse) * texture(tex, In.texCoords).xyz * color;
	//FragColor = vec4(result, alpha);

	FragColor = phong(In.fragPos, In.normal, normalize(camPos - In.fragPos));
}
