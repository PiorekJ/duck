﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in vec2 TexCoord;
in vec3 Normal;
in vec3 WorldPos;
in vec3 LocalPos;
//in vec3 View;

uniform sampler2D bumpTex;
uniform samplerCube envTex;
uniform vec3 color;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 camPos;
uniform float ks, kd, ka, m, n1, n2;

vec4 phong(vec3 worldPos, vec3 norm, vec3 view)
{
	view = normalize(view);
	norm = normalize(norm);
	vec3 color = color * ka; //ambient
	vec3 lightVec = normalize(lightPos - worldPos);
	vec3 halfVec = normalize(view + lightVec);
	color += lightColor * kd * color * clamp(dot(norm, lightVec), 0.0, 1.0);//diffuse
	color += lightColor * ks * pow(clamp(dot(norm, halfVec), 0.0, 1.0), m);//specular
	return clamp(vec4(color, 1.0f), 0.0, 1.0);
}

vec3 normalMapping(vec3 N, vec3 T, vec3 tn)
{
	vec3 B = normalize(cross(N, T));
	T = cross(B, N);
	return transpose(mat3(T, B, N)) * tn;
}

vec3 intersectRay(vec3 P, vec3 D)
{
	vec3 F = max((1 - P)/ D, (-1 - P) / D);

	float m = min(F.x, F.y);

	return P + min(m, F.z)*D;
}


float fresnel(vec3 N, vec3 V)
{
	float F0 = pow((n2 - n1) / (n2 + n1), 2);
	return F0 + (1 - F0)*pow(1 - max(dot(N, V), 0), 5);
}

void main()
{
	vec3 view = normalize(camPos - WorldPos.xyz);
	vec3 dPdx = dFdx(WorldPos);
	vec3 dPdy = dFdy(WorldPos);
	vec2 dtdx = dFdx(TexCoord);
	vec2 dtdy = dFdy(TexCoord);
	
	vec3 T = normalize(-dPdx * dtdy.y + dPdy * dtdx.y);
	vec3 tn = texture(bumpTex, TexCoord).rgb;

	tn = 2 * tn - vec3(1, 1, 1);

	vec3 norm = normalize(normalMapping(Normal, T, tn));

	vec3 reflectVector = reflect(-view, norm);
	vec3 refractVector;
	if (camPos.y > WorldPos.y)
	{
		refractVector = refract(-view, norm, n1 / n2);
	}
	else
	{
		norm = -norm;
		refractVector = refract(-view, norm, n2 / n1);
		refractVector.y *= -1;
		norm = -norm;
	}

	vec3 reflectEnv = texture(envTex, intersectRay(LocalPos, reflectVector)).rgb;
	vec3 refractEnv = texture(envTex, intersectRay(LocalPos, refractVector)).rgb;

	float fres = fresnel(norm, view);

	vec3 color;

	//if (abs(refractVector.x) > 0.00001f || abs(refractVector.y) > 0.00001f || abs(refractVector.z) > 0.00001f)
	if(refractVector != vec3(0,0,0))
	{
		color = mix(refractEnv, reflectEnv, fres);
	}
	else
		color = reflectEnv;

	color.x = pow(color.x, 0.4545f);
	color.y = pow(color.y, 0.4545f);
	color.z = pow(color.z, 0.4545f);

	//FragColor = vec4(intersectRay(LocalPos, refractVector).rgb, 1.0f);
	FragColor = vec4(color, 1.0f);
}
