﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 normal;
in layout(location = 2) vec2 texCoord;

uniform mat4 model;
uniform mat4 modelInv;
uniform mat4 view;
uniform mat4 projection;


out vec2 TexCoord;
out vec3 Normal;
out vec3 WorldPos;
out vec3 LocalPos;
//out vec3 View;

void main()
{
	vec4 worldPos = (model * vec4(position, 1.0f));
	TexCoord = texCoord;	
	Normal = normalize((modelInv * vec4(normal, 0.0f)).xyz);
	WorldPos = worldPos.xyz;
	LocalPos = position;
	LocalPos.y = 0.0f;
	gl_Position = projection * view * worldPos;
}
