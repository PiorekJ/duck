﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Duck.Core;
using Duck.OpenTK;
using Duck.Utils;
using OpenTK;
using OpenTK.Graphics.ES11;
using PureCAD.Utils;

namespace Duck.Models
{
    public class EnviromentModel : Model
    {
        private Texture _texture;
        
        public EnviromentModel()
        {
            Mesh = GeneratePlaneMesh();
            Shader = Shaders.CubeMapShader;
            _texture = Textures.CubeMapTexture;
            Transform.Scale = new Vector3(Simulation.Settings.SceneSize / 2.0f);
        }

        private Mesh<VertexP> GeneratePlaneMesh()
        {
            List<VertexP> vertices = new List<VertexP>();
            
            var size = Simulation.Settings.SceneSize / 2.0f;

            vertices.Add(new VertexP(-1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, -1.0f));

            vertices.Add(new VertexP(-1.0f, -1.0f, 1.0f));
            vertices.Add(new VertexP(-1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(-1.0f, -1.0f, 1.0f));

            vertices.Add(new VertexP(1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, -1.0f));

            vertices.Add(new VertexP(-1.0f, -1.0f, 1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, 1.0f));
            vertices.Add(new VertexP(-1.0f, -1.0f, 1.0f));

            vertices.Add(new VertexP(-1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, 1.0f));
            vertices.Add(new VertexP(-1.0f, 1.0f, -1.0f));

            vertices.Add(new VertexP(-1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, -1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, -1.0f));
            vertices.Add(new VertexP(-1.0f, -1.0f, 1.0f));
            vertices.Add(new VertexP(1.0f, -1.0f, 1.0f));

            
            return new Mesh<VertexP>(vertices, null, MeshType.Triangles, AccessType.Static);
        }

        protected override void OnRender()
        {
            GL.Enable(EnableCap.CullFace);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.DrawTexture(_texture.TexID);
            GL.Disable(EnableCap.CullFace);
        }
    }
}
