﻿using System.Collections.Generic;
using System.Windows.Media;
using Duck.Core;
using Duck.OpenTK;
using OpenTK;

namespace Duck.Models
{
    public class LightModel : Model
    {
        public Color Color { get; set; } = Colors.White;

        public LightModel()
        {
            Transform.Position = new Vector3(-2, 20, 5);
            Transform.Scale = new Vector3(0.25f);
            Shader = Shaders.LightShader;
            Mesh = GenerateCubeMesh();
        }
        
        protected override void OnUpdate()
        {
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.Draw();
        }

        private Mesh<VertexP> GenerateCubeMesh()
        {
            List<VertexP> vertices = new List<VertexP>()
            {
                new VertexP(-1.0f, -1.0f, 1.0f),
                new VertexP(1.0f, -1.0f, 1.0f),
                new VertexP(1.0f, 1.0f, 1.0f),
                new VertexP(-1.0f, 1.0f, 1.0f),
                new VertexP(-1.0f, -1.0f, -1.0f),
                new VertexP(1.0f, -1.0f, -1.0f),
                new VertexP(1.0f, 1.0f, -1.0f),
                new VertexP(-1.0f, 1.0f, -1.0f)
            };
            List<uint> edges = new List<uint>()
            {
                0,
                1,
                2,
                2,
                3,
                0,

                1,
                5,
                6,
                6,
                2,
                1,

                7,
                6,
                5,
                5,
                4,
                7,

                4,
                0,
                3,
                3,
                7,
                4,

                4,
                5,
                1,
                1,
                0,
                4,

                3,
                2,
                6,
                6,
                7,
                3
            };

            return new Mesh<VertexP>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }
    }
}
