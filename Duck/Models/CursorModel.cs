﻿using System.Collections.Generic;
using Duck.Components;
using Duck.Core;
using Duck.OpenTK;
using OpenTK;

namespace Duck.Models
{
    public class CursorModel : Model
    {
        public CursorModel()
        {
            Transform.Position = Vector3.Zero;
            Shader = Shaders.BasicShader;
            Mesh = GenerateCursorMesh();
            IsVisibleOnList = false;
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.Draw();
        }

        protected override void OnUpdate()
        {
        }

        private Mesh<VertexPC> GenerateCursorMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.UnitX * 0.5f, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.UnitY * 0.5f, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitZ));
            vertices.Add(new VertexPC(Vector3.UnitZ * 0.5f, Vector3.UnitZ));

            return new Mesh<VertexPC>(vertices, null, MeshType.Lines, AccessType.Static);
        }
    }
}
