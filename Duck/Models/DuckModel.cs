﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Duck.Core;
using Duck.OpenTK;
using Duck.Utils;
using Duck.Utils.Math;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Utils;

namespace Duck.Models
{
    public class DuckModel : Model
    {
        private Texture _texture;
        private Vector2 _waterPosition;
        private float _time;
        private Vector3[] _splinePoints;
        private Random _random;
        private Matrix4 _rotationMatrix = Matrix4.Identity;
        private float _duckSubmerge = -0.05f;

        public Vector2 WaterPosition
        {
            get
            {
                return new Vector2((Transform.Position.X*2 / Simulation.Settings.SceneSize + 1)/2.0f * 256, (-Transform.Position.Z*2 / Simulation.Settings.SceneSize + 1)/2.0f * 256);
            }
        }

        public DuckModel()
        {
            _random = new Random();
            _splinePoints = new Vector3[] { RandomizeSplinePoint(), RandomizeSplinePoint(), RandomizeSplinePoint(), RandomizeSplinePoint() };
            _texture = Textures.DuckTexture;
            Mesh = MeshManager.ReadMeshFile("./MeshFiles/Duck.txt");
            Shader = Shaders.LitTexturedObjectShader;
            Transform.Scale = Vector3.One * 0.0025f;

        }

        private float min = -Simulation.Settings.SceneSize / 2.0f + 0.1f;
        private float max = Simulation.Settings.SceneSize / 2.0f - 0.1f;

        private Vector3 RandomizeSplinePoint()
        {
            return new Vector3((float)_random.NextDouble() * (max - min) + min, _duckSubmerge, (float)_random.NextDouble() * (max - min) + min);
            //return new Vector3(_random.Next(-(int)Simulation.Settings.SceneSize / 2 + 1, (int)Simulation.Settings.SceneSize / 2 - 1), _duckSubmerge, _random.Next(-(int)Simulation.Settings.SceneSize / 2 + 1, (int)Simulation.Settings.SceneSize / 2 - 1));
        }

        Vector3 DeBoor(float t, Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            float[] N = new float[4];
            int i, j;
            float saved, term;
            N[0] = 1;
            for (i = 1; i < 4; i++)
            {
                saved = 0;
                for (j = 0; j < i; j++)
                {
                    term = N[j] / i;
                    N[j] = saved + (j - t + 1) * term;
                    saved = (t + i - 1 - j) * term;
                }
                N[i] = saved;
            }

            return N[0] * a + N[1] * b + N[2] * c + N[3] * d;
        }

        protected override void OnUpdate()
        {
            _time += Simulation.DeltaTime * Simulation.Settings.DuckSpeed;
            if (_time - Constants.Epsilon >= 1)
            {
                _time = 0;
                _splinePoints[0] = _splinePoints[1];
                _splinePoints[1] = _splinePoints[2];
                _splinePoints[2] = _splinePoints[3];
                _splinePoints[3] = RandomizeSplinePoint();
                return;
            }
            var currPosition = DeBoor(_time, _splinePoints[0], _splinePoints[1], _splinePoints[2], _splinePoints[3]);
            var nextPosition = DeBoor(_time + Constants.Epsilon, _splinePoints[0], _splinePoints[1], _splinePoints[2], _splinePoints[3]);

            var tangent = (nextPosition - currPosition).Normalized();
            var axis = Vector3.Cross(-Vector3.UnitX, tangent);
            var radAngle = (float)Math.Acos(Vector3.Dot(-Vector3.UnitX, tangent));
            var rot = Quaternion.FromAxisAngle(axis, radAngle);
            if(axis == Vector3.Zero)
                rot = Quaternion.Identity;
            
            Transform.Position = currPosition;
            Transform.Rotation = rot;

        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.Light.Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.Light.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("ks"), 0.4f);
            Shader.Bind(Shader.GetUniformLocation("kd"), 0.8f);
            Shader.Bind(Shader.GetUniformLocation("ka"), 0.2f);
            Shader.Bind(Shader.GetUniformLocation("m"), 100.0f);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _texture.TexID);
            Shader.Bind(Shader.GetUniformLocation("tex"), 0);
            Mesh.Draw();
        }
    }
}
