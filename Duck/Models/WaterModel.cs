﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Duck.Components;
using Duck.Core;
using Duck.OpenTK;
using Duck.Utils;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Utils;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace Duck.Models
{
    public class WaterModel : Model
    {
        private int _bumpTexture;
        private int _envTexture;

        private float[] _currentHeightMap;
        private float[] _previousHeightMap;
        private float[] _bumpMap;
        private float[] _d;
        private float _c = 1.0f;
        private float _h;
        private float _dT;
        private Random _random;

        private float A;
        private float B;

        private int _heightMapSize = 256;

        private Matrix4 _modelMatrix;
        private Matrix4 _modelMatrixInv;

        public WaterModel()
        {
            _currentHeightMap = new float[_heightMapSize * _heightMapSize];
            _d = new float[_heightMapSize * _heightMapSize];
            for (int i = 0; i < _heightMapSize; i++)
            {
                for (int j = 0; j < _heightMapSize; j++)
                {
                    var side = new Vector2(((i / _heightMapSize * 2) - 1) * 2, ((j / _heightMapSize * 2) - 1) * 2);
                    float l = 0.0f;
                    if (side.X < 0 && side.Y < 0)
                    {
                        l = Math.Min((-2 * Vector2.UnitX - side).Length, (-2 * Vector2.UnitY - side).Length);
                    }
                    if (side.X >= 0 && side.Y < 0)
                    {
                        l = Math.Min((2 * Vector2.UnitX - side).Length, (-2 * Vector2.UnitY - side).Length);
                    }
                    if (side.X >= 0 && side.Y >= 0)
                    {
                        l = Math.Min((2 * Vector2.UnitX - side).Length, (2 * Vector2.UnitY - side).Length);
                    }
                    if (side.X < 0 && side.Y >= 0)
                    {
                        l = Math.Min((-2 * Vector2.UnitX - side).Length, (2 * Vector2.UnitY - side).Length);
                    }

                    _d[i + j * _heightMapSize] = (float)(0.95 * Math.Min(1.0, l / 2.0));
                }
            }
            _previousHeightMap = new float[_heightMapSize * _heightMapSize];
            _bumpMap = new float[_heightMapSize * _heightMapSize * 3];
            _h = 2.0f / (_heightMapSize - 1);
            _dT = 1.0f / _heightMapSize;
            A = ((_c * _c) * (_dT * _dT)) / (_h * _h);
            B = 2 - 4 * A;
            _random = new Random();
            Transform.Scale = Vector3.One * Simulation.Settings.SceneSize/2.0f;
            _modelMatrix = Transform.GetModelMatrix();
            _modelMatrixInv = Matrix4.Invert(_modelMatrix);
            _envTexture = Textures.CubeMapTexture.TexID;
            Shader = Shaders.WaterShader;
            Mesh = GeneratePlaneMesh();
            GenerateBumpTexture();
            
        }

        private void CalculateHeightMapData()
        {
            for (int i = 0; i < _heightMapSize; i++)
            {
                for (int j = 0; j < _heightMapSize; j++)
                {
                    var d = _d[i + j * _heightMapSize];
                    if (i > 0 && i < _heightMapSize - 1 && j > 0 && j < _heightMapSize - 1)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i + 1) + _heightMapSize * j] +
                                      _currentHeightMap[(i - 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j - 1)] +
                                      _currentHeightMap[i + _heightMapSize * (j + 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i == 0 && j == 0)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i + 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j + 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i == 0 && j == _heightMapSize - 1)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i + 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j - 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i == _heightMapSize - 1 && j == 0)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i - 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j + 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i == _heightMapSize - 1 && j == _heightMapSize - 1)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i - 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j - 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i == 0 && j != 0 && j != _heightMapSize - 1)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i + 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j - 1)] +
                                      _currentHeightMap[i + _heightMapSize * (j + 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i == _heightMapSize - 1 && j != 0 && j != _heightMapSize - 1)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i - 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j - 1)] +
                                      _currentHeightMap[i + _heightMapSize * (j + 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i != 0 && i != _heightMapSize - 1 && j == 0)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i + 1) + _heightMapSize * j] +
                                      _currentHeightMap[(i - 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j + 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else if (i != 0 && i != _heightMapSize - 1 && j == _heightMapSize - 1)
                    {
                        _previousHeightMap[i + _heightMapSize * j] =
                            d * (A * (_currentHeightMap[(i + 1) + _heightMapSize * j] +
                                      _currentHeightMap[(i - 1) + _heightMapSize * j] +
                                      _currentHeightMap[i + _heightMapSize * (j - 1)]) +
                                 B * _currentHeightMap[i + _heightMapSize * j] -
                                 _previousHeightMap[i + _heightMapSize * j]);
                    }
                    else
                    {
                        throw new AccessViolationException("Out of bounds");
                    }
                }
            }
            _currentHeightMap = _previousHeightMap;
        }

        private void CalculateBumpMapData()
        {
            float diff, x, y, z, length;
            float zScale = 1.0f/_heightMapSize;
            int count = 0;
            for (int i = 0; i < _heightMapSize; i++)
            {
                for (int j = 0; j < _heightMapSize; j++)
                {
                    x = y = z = 0.0f;
                    if (i > 0)
                    {
                        if (j > 0)
                        {
                            diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                            _currentHeightMap[i - 1 + (j-1) * _heightMapSize]);
                            x -= diff;
                            y -= diff;
                            z += zScale;
                        }

                        diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                        _currentHeightMap[i - 1 + j * _heightMapSize]);
                        y -= diff;
                        z += zScale;

                        if (j < _heightMapSize - 1)
                        {
                            diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                            _currentHeightMap[i - 1 + (j + 1) * _heightMapSize]);
                            x += diff;
                            y -= diff;
                            z += zScale;
                        }
                    }

                    if (i < _heightMapSize - 1)
                    {
                        if (j > 0)
                        {
                            diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                            _currentHeightMap[i + 1 + (j - 1) * _heightMapSize]);
                            x -= diff;
                            y += diff;
                            z += zScale;
                        }

                        diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                        _currentHeightMap[i + 1 + j * _heightMapSize]);
                        y += diff;
                        z += zScale;

                        if (j < _heightMapSize - 1)
                        {
                            diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                            _currentHeightMap[i + 1 + (j + 1) * _heightMapSize]);
                            x += diff;
                            y += diff;
                            z += zScale;
                        }
                    }

                    if (j > 0)
                    {
                        diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                        _currentHeightMap[i + (j - 1) * _heightMapSize]);
                        x -= diff;
                        z += zScale;
                    }

                    if (j < _heightMapSize - 1)
                    {
                        diff = Math.Abs(_currentHeightMap[i + j * _heightMapSize] -
                                        _currentHeightMap[i + (j + 1) * _heightMapSize]);
                        x += diff;
                        z += zScale;
                    }


                    length = (float)Math.Sqrt(x * x + y * y + z * z);
                    x /= length;
                    y /= length;
                    z /= length;

                    _bumpMap[i * 3 + j * 3 * _heightMapSize] = (x + 1) / 2;
                    _bumpMap[i * 3 + 1 + j * 3 * _heightMapSize] = (y + 1) / 2;
                    _bumpMap[i * 3 + 2 + j * 3 * _heightMapSize] = (z + 1) / 2;
                }
            }
        }

        private void GenerateBumpTexture()
        {
            _bumpTexture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, _bumpTexture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, _heightMapSize, _heightMapSize, 0, PixelFormat.Rgb, PixelType.Float, _bumpMap);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            // GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            // GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
        }

        private Mesh<VertexPNT> GeneratePlaneMesh()
        {
            List<VertexPNT> vertices = new List<VertexPNT>();
            vertices.Add(new VertexPNT(-1.0f, 0, 1.0f, Vector3.UnitY, 0, 0));
            vertices.Add(new VertexPNT(-1.0f, 0, -1.0f, Vector3.UnitY, 0, 1));
            vertices.Add(new VertexPNT(1.0f, 0, -1.0f, Vector3.UnitY, 1, 1));
            vertices.Add(new VertexPNT(1.0f, 0, 1.0f, Vector3.UnitY, 1, 0));

            List<uint> edges = new List<uint>();

            edges.Add(2);
            edges.Add(1);
            edges.Add(0);

            edges.Add(3);
            edges.Add(2);
            edges.Add(0);

            return new Mesh<VertexPNT>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        private float _dropTime = 0.0f;

        protected override void OnUpdate()
        {
            _dropTime += Simulation.DeltaTime;
            if (_dropTime > 0.05f)
            {
                _dropTime = 0;
                _currentHeightMap[_random.Next(0, _heightMapSize) + _random.Next(0, _heightMapSize) * _heightMapSize] = 5.0f;
            }

            _currentHeightMap[(int)Simulation.Scene.Duck.WaterPosition.X + (int)Simulation.Scene.Duck.WaterPosition.Y *
                _heightMapSize] = 10.0f;

            CalculateHeightMapData();
            CalculateBumpMapData();
            GL.BindTexture(TextureTarget.Texture2D, _bumpTexture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, _heightMapSize, _heightMapSize, 0, PixelFormat.Rgb, PixelType.Float, _bumpMap);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), _modelMatrix);
            Shader.Bind(Shader.GetUniformLocation("modelInv"), _modelMatrixInv);
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.Light.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.Light.Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("color"), Colors.CornflowerBlue.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("ks"), 0.8f);
            Shader.Bind(Shader.GetUniformLocation("kd"), 0.5f);
            Shader.Bind(Shader.GetUniformLocation("ka"), 0.2f);
            Shader.Bind(Shader.GetUniformLocation("m"), 100.0f);
            Shader.Bind(Shader.GetUniformLocation("n1"), 1.0f);
            Shader.Bind(Shader.GetUniformLocation("n2"), 4.0f/3.0f);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _bumpTexture);
            Shader.Bind(Shader.GetUniformLocation("bumpTex"), 0);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.TextureCubeMap, _envTexture);
            Shader.Bind(Shader.GetUniformLocation("envTex"), 1);
            Mesh.Draw();
        }
    }
}
