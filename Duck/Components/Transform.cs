﻿using Duck.Core;
using Duck.Utils.Math;
using OpenTK;

namespace Duck.Components
{
    public class Transform : Component
    {
        private Vector3 _position = Vector3.Zero;
        private Quaternion _rotation = Quaternion.Identity;
        private Vector3 _scale = Vector3.One;

        public delegate void UpdateEventHandler();

        private event UpdateEventHandler OnPositionUpdate;
        private event UpdateEventHandler OnRotationUpdate;
        private event UpdateEventHandler OnScaleUpdate;

        public Vector3 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                RaisePropertyChanged();
                OnPositionUpdate?.Invoke();
            }
        }

        public Quaternion Rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value;
                RaisePropertyChanged();
                OnRotationUpdate?.Invoke();
            }
        }

        public Vector3 Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                RaisePropertyChanged();
                OnScaleUpdate?.Invoke();
            }
        }

        public Matrix4 GetModelMatrix()
        {
            return MathGeneralUtils.CreateScale(Scale) * MathGeneralUtils.CreateFromQuaternion(Rotation) * MathGeneralUtils.CreateTranslation(Position);
        }

        public Transform()
        { }

        public Transform(UpdateEventHandler onPositionUpdate, UpdateEventHandler onRotationUpdate, UpdateEventHandler onScaleUpdate)
        {
            OnPositionUpdate += onPositionUpdate;
            OnRotationUpdate += onRotationUpdate;
            OnScaleUpdate += onScaleUpdate;
        }

    }
}
